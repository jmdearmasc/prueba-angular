import { Component, OnInit,  Input } from '@angular/core';
import { NavItem } from '../../models/navigation';
import { NavigationService } from '../../navigation.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  menu: NavItem[];

  @Input()
    localId: string;

  constructor(private navigationService: NavigationService ) { }

  ngOnInit(): void {
    this.getMenu();
    const a = ()=>{};
    const b = function(){};
   
  }

  getMenu():void{
    this.navigationService.getMenu()
    .subscribe(items=>{ this.menu = items; console.log(items)});
  }
  

  
}
