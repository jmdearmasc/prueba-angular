import { Component, OnInit } from '@angular/core';
import { dataset, row } from '../../models/dataset';


@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements OnInit {

  fields: Array<string>;
  rows: Array<row>;

  constructor() { 

  }

  ngOnInit(): void {
  }


  prepareFields(data: dataset){
    this.fields = data.fields;
  }


  

}
