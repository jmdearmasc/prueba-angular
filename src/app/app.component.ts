import { Component, LOCALE_ID, Inject } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  languageList = [    { code: 'en', label: 'English' }, 
                      { code: 'es', label: 'Espanol' }  ]; 
  title = 'Tutorial de Angular, Cambio ... ';
  locale: string;

  constructor(@Inject(LOCALE_ID) protected localeId: string){

    console.log(localeId);
    this.locale = localeId;

  }
}
