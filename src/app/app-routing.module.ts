import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HolidaysComponent } from './holidays/holidays.component';
import { DefaultComponent } from './default/default.component';
import { VideosComponent } from './videos/videos.component';


const routes: Routes = [
 { path: '', component: DefaultComponent },
  { path: 'holidays', component: HolidaysComponent },
  { path: 'videos', component: VideosComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
