export interface NavItem{
    LINK: string,
    ES_DESCRIPTION: string,
    EN_DESCRIPTION: string,
    PARENT_ID: number,
    CHILD_ID: number,
    CHILDREN: Array<NavItem>,
    HAS_CHILDREN: boolean
}


