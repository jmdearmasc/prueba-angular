export interface holiday {
    HOLIDAY_ID: number,
    DESCRIPTION: string,
    HOLIDAY_DATE: number,
    STATUS: string,
    COMPANY_ID: number
}


export interface holidays{
    items: Array<holiday>
}