export interface dataset {
    fields: Array<string>,
    rows: Array<row>
}


export interface row {
    values: Array<any>
}