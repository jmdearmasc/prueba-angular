import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { DataTablesModule } from 'angular-datatables';
import { DatatableComponent } from './shared/datatable/datatable.component';
import { DefaultComponent } from './default/default.component';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { VideosComponent } from './videos/videos.component';

@NgModule({
  declarations: [
    AppComponent,
    HolidaysComponent,
    DatatableComponent,
    DefaultComponent,
    NavigationComponent,
    VideosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DataTablesModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
