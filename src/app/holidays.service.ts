import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { holiday } from './models/holiday';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const GETALLURL = "http://192.168.101.81:82/renova.asmx/CO_HOLIDAYS_MASTER_GET_ALL";
const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json',
    'method': 'POST' })
 
};

@Injectable({
  providedIn: 'root'
})
export class HolidaysService {

  constructor(private http: HttpClient) { }

  getAll(id:number):Observable<holiday[]> {
    return this.http.post<any>(GETALLURL, {companyId : 1}, httpOptions );
  }
}
