import { Component, OnInit } from '@angular/core';
import { DatatableComponent } from '../shared/datatable/datatable.component';
import { holiday } from '../models/holiday';
import { HolidaysService } from '../holidays.service';
@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.css']
})
export class HolidaysComponent implements OnInit {

  public holidayInstance: holiday;
  holidays: holiday[];
  constructor(private holidaysService: HolidaysService) { }

  ngOnInit(): void {
    this.holidaysService.getAll(1).subscribe(items=>{console.log(items); this.holidays = JSON.parse(items.d) })
  }

}
