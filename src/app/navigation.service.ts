import { Injectable } from '@angular/core';
import MenuItems from '../assets/menuitems.json';
import { Observable, of } from 'rxjs';
import { NavItem } from './models/navigation';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor() { }

  getMenu():Observable<NavItem[]>{
    return of(MenuItems);
  }
}
